<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('profile', 'UserController@profile')->name('user.profile');
    Route::post('profile', 'UserController@profile_update')->name('user.profile.update');
    Route::post('demo', 'UserController@demo')->name('user.demo');

    Route::get('userprofile', 'Usermanege@profile')->name('user.userprofile');
    Route::post('userprofile', 'Usermanege@profile_update')->name('user.userprofile.update');



    Route::get('roles', 'RoleController@index')->name('roles.index');
    Route::get('roles/create', 'RoleController@create')->name('roles.create');
    Route::post('roles/create', 'RoleController@store')->name('roles.store');
    Route::get('roles/edit', 'RoleController@edit')->name('roles.edit');
    Route::post('roles/edit', 'RoleController@update')->name('roles.update');
    Route::post('roles/destroy', 'RoleController@destroy')->name('roles.destroy');
});

