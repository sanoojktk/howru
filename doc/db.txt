

role
id
name
type


permission
role_id
permission


type
private
public




user
id
name
email
email_verified_at
mobile
secondary_mobile
address
password
image


































service_category
id
name
image

service
id
service_category_id
name
image
details

location
id
name

partner
id
name
mobile
logo
description

partner_location
id
partner_id
location_id
address
settlement_days

partner_location_service
id
partner_location_id
service_id
buying_amount
selling_amount
commission


campaign
id
code
name
description
discount_percentage 


campaign_service
id
campaign_id
partner_location_service_id






call
id
type [incoming / outgoing]
name
mobile
secondary_mobile
email
campaign_id
order_id
remark
created_by


order
id
name
mobile
secondary_mobile
email
address
remark
campaign_id
total_amount
received_amount
payment_method
created_method [web/app/call]
created_by


order_item
order_id
partner_id*
partner_location_id*
partner_location_service_id
partner_name
partner_image
partner_details
customer_name
customer_age
name
qty
image
remark
booking_date
booking_time

campaign_id

buying_amount  
selling_amount
discount_amount
commission_amount
settlement_date
status


order_status
details
datetime
status


callback
id
order_id
support_id
date
time
remark
created_method [web/app/call]
created_by