<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\user;

class Usermanege extends Controller
{
    public function profile(){

        $user = Auth::user();

        return view('users/userprofile', compact('user'));
    }       

    public function profile_update(Request $request) {  

        $user = Auth::user();

        request()->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users,email,' . $user->id . ',id',],
        ]);

        DB::beginTransaction();
            
        try{
          
           $input = $request->only(['name', 'email', 'mobile', 'secondary_mobile', 'address']);
 
           $user->update( $input ); 

        } catch(\Exception $e){

            DB::rollback();

            return response()->json([
                'alert' => [
                    'icon' => 'error',
                    'title' => 'Profile',
                    'title' => 'Something Went Wrong.'
                ]                
            ]);
        }

        DB::commit();

        return response()->json([
            'alert' => [
                'icon' => 'success',
                'title' => 'Profile',
                'text' => 'Updated Successfullys.',
            ],
            'ui' => [     
                [
                    'element' => '#profile-name',
                    'method'  => 'html',
                    'value'   => $user->name
                ],

                [
                    'element' => '#profile-image .avatar-title',
                    'method'  => 'html',
                    'value'   =>  Str::upper( Str::limit(Auth::user()->name, 1, '') )
                ],

                [
                    'element' => '.vertical-menu-btn i',
                    'method'  => 'removeClass',
                    'value'   => 'uil-web-section-alt'
                ],

                [
                    'element' => '.vertical-menu-btn i',
                    'method'  => 'addClass',
                    'value'   => 'uil-web-section'
                ],
            ]              
        ]); 
    }

    public function demo(Request $request){

        sleep(4);
        return response()->json([
            'ui' => [
               'element' => '#' . $request->id,
               'method'  => 'html',
               'value'   => 33
            ],  
        ]); 
    }    

}
