const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(
    'resources/assets/js/theme.js',
        'public/assets/js'
    )
    .js(
        'resources/assets/js/app.js', 
        'public/assets/js'
    )
    .sass(
        'resources/assets/scss/app.scss', 
        'public/assets/css'
    )
    .sourceMaps(true, 'source-map')
    .version()
    .browserSync('http://howryou.test');