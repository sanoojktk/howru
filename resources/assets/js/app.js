var app = {};
try {
    window.Swal = require('sweetalert2')
} catch (e) {}

'use strict';
(function ($) {

    $( document ).ready(function() {
 
        app.ajax = {};

        $('[ajax="form"]').on('submit', function(){
            event.preventDefault();
            let element = this;
            let data = new FormData(this);
            let url  = $(this).attr("action");
            app.ajax.request(element, url, data);    
        });

        $('[ajax="click"]').on('click', function(){
            event.preventDefault();
            let element = this;
            let data = new FormData();
            let url = $(this).attr("action");
            $.each(this.attributes, function() {
                data.append(this.name, this.value);
            });
            app.ajax.request(element, url, data);
        });
 
        $(window).on('load', function() {
            if($('[ajax="load"]').length > 0) {
                $('[ajax="load"]').each(function(){
                    let element = this;
                    let data = new FormData();
                    let url = $(this).attr("action");
                    $.each(this.attributes, function() {
                        data.append(this.name, this.value);
                    });
                    app.ajax.request(element, url, data);
                });
            }
        });
        
        app.ajax.request = function (element, url, data) {

            $(element).addClass('processing');
            $('[type="submit"]', element).prop('disabled', true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: 'post',
                data: data,
                contentType: false,
                cache: false,
                processData: false
            }).done(function (response) {
                app.ajax.response(element, response);
                $(element).removeClass('processing');
                $('[type="submit"]', element).prop('disabled', false);
            }).fail(function (jqXHR, textStatus) {  
                if(jqXHR.status == 422){
                    app.ajax.response(element, jqXHR.responseJSON);
                }else{
                    //location.reload();
                }
                $(element).removeClass('processing');
                $('[type="submit"]', element).prop('disabled', false);
            });
        }

        app.ajax.response = function (element, response) {
            $('.is-invalid', element).removeClass('is-invalid').next('.invalid-feedback').remove();

            // Reset
            if (response.reset) {
               $(element).trigger("reset");
            }

            // Form Errors
            if (response.errors) {
                $.each(response.errors, function (field, errors) {
                    $("[name='" + field + "']", element).addClass("is-invalid");
                    $("[name='" + field + "']", element).after( $('<div/>').addClass('invalid-feedback').html(errors[0]) );
                
                    $("[name='" + field + "']", element).on('change', function(){
                        $(this).removeClass('is-invalid').next('.invalid-feedback').remove();
                    });
                });
            }

            // Ui Update
            if (response.ui) {

                if (response.ui.element && response.ui.method && response.ui.value) {
                    $(response.ui.element).[response.ui.method](response.ui.value);
                }else{
                    $.each(response.ui, function (index, uiTarget) {
                        if (uiTarget.element && uiTarget.method && uiTarget.value) {
                            $(uiTarget.element).[uiTarget.method](uiTarget.value);
                        }
                    });
                }
            }

            // Redirect
            if (response.redirect) {
                window.location.href = response.redirect;
            }

            // Alert
            if (response.alert) {
                let swalconfig = {
                    position: 'center',
                    showConfirmButton: false,
                    timer: 2000
                }

                Object.assign(swalconfig, response.alert)

                Swal.fire(swalconfig);
            }            
        };

        $('[type="submit"]').prop('disabled', false);
    });

})(jQuery)