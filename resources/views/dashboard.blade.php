@extends('layouts.app')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0">Dashboard</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">User</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
 
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1">
                            <span id="demo1" ajax="load" action="{{ route('user.demo') }}">
                                <img src="{{ asset('assets/images/loader.svg') }}" alt="loader" width="24">
                            </span>
                        </h4>
                        <p class="text-muted mb-0">{{ __('Open Orders') }}</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1"><span  id="demo2" ajax="click" action="{{ route('user.demo') }}">55</span></h4>
                        <p class="text-muted mb-0">{{ __('Processing Orders') }}</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1"><span data-plugin="counterup">43</span></h4>
                        <p class="text-muted mb-0">{{ __('Closed Orders') }}</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">

            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1"><span data-plugin="counterup">22</span></h4>
                        <p class="text-muted mb-0">{{ __('Cancelled Orders') }}</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->
    </div> <!-- end row-->



@endsection
