@extends('layouts.app')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0">Profile</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">User</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
 
                <form method="post" class="local-validation" action="{{ route('user.userprofile.update') }}" ajax="form">
 
                    <div class="row">
                        <div class="col-lg-8 col-sm-12">

                        <div class="form-group">
                            <label>{{ __('Name') }}</label>
                            <div>
                                <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <label>{{ __('Email') }}</label>
                            <div>
                                <input type="email" name="email" value="{{ $user->email }}"  class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{ __('Mobile') }}</label>
                            <div>
                                <input type="text" name="mobile" value="{{ $user->mobile }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{ __('Secondary Mobile') }}</label>
                            <div>
                                <input type="text" name="secondary_mobile" value="{{ $user->secondary_mobile }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{ __('Address') }}</label>
                            <div>
                                <textarea  class="form-control" name="address" rows="3">{{ $user->address }}</textarea>
                            </div>
                        </div>

                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" disabled="disabled" class="btn btn-primary waves-effect waves-light mr-1">
                                    {{ __('Save') }}
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect">                         
                                    {{ __('Cancel')}}
                                </button>
                            </div>
                        </div>


                        </div>
                    </div>

                </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection
