<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <!-- App Css-->
        <link href="{{ mix('assets/css/app.css') }}" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body id="app">

    <!-- <body data-layout="horizontal" data-topbar="colored"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">

            
            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="{{ url('/') }}" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="{{ asset('assets/images/logo-sm.png') }}" alt="{{ config('app.name', 'Laravel') }}" height="40">
                                </span>
                                <span class="logo-lg">
                                    <img src="{{ asset('assets/images/logo-dark.png') }}" alt="{{ config('app.name', 'Laravel') }}" height="40">
                                </span>
                            </a>
                        </div>
 
                        <!-- App Search-->
                        <form class="app-search d-none d-lg-block">
                            <div class="position-relative">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="uil-search"></span>
                            </div>
                        </form>
                    </div>

                    <div class="d-flex">
 

                            <div class="dropdown d-inline-block">
                                <button type="button" class="btn header-item noti-icon waves-effect vertical-menu-btn">
                                    <i class="uil uil-web-section-alt"></i>
                                </button>
                            </div>

                            <div class="dropdown d-none d-lg-inline-block">
                                <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                    <i class="uil uil-expand-arrows-alt"></i>
                                </button>
                            </div>


                            <div class="dropdown d-inline-block">
                                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="uil-bell"></i>
                                    <span class="badge badge-danger badge-pill">3</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                                    aria-labelledby="page-header-notifications-dropdown">
                                    <div class="p-3">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <h5 class="m-0 font-size-16"> Notifications </h5>
                                            </div>
                                            <div class="col-auto">
                                                <a href="#!" class="small"> Mark all as read</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-simplebar style="max-height: 230px;">
                                        <a href="" class="text-reset notification-item">
                                            <div class="media">
                                                <div class="avatar-xs mr-3">
                                                    <span class="avatar-title bg-primary rounded-circle font-size-16">
                                                        <i class="uil-shopping-basket"></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="mt-0 mb-1">Your order is placed</h6>
                                                    <div class="font-size-12 text-muted">
                                                        <p class="mb-1">If several languages coalesce the grammar</p>
                                                        <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="" class="text-reset notification-item">
                                            <div class="media">
                                                <div class="avatar-xs mr-3">
                                                    <span class="avatar-title bg-success rounded-circle font-size-16">
                                                        <i class="uil-truck"></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="mt-0 mb-1">Your item is shipped</h6>
                                                    <div class="font-size-12 text-muted">
                                                        <p class="mb-1">If several languages coalesce the grammar</p>
                                                        <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="p-2 border-top">
                                        <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                                            <i class="uil-arrow-circle-right mr-1"></i> View More..
                                        </a>
                                    </div>
                                </div>
                            </div>
 
                            <div class="dropdown d-inline-block">
                                <button id="profile-image" type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <!--  -->
                                    @if(Auth::user()->image == '')
                                    <div class="avatar-xs d-inline-block mr-2">
                                        <span class="avatar-title rounded-circle bg-primary">
                                        {{ Str::upper( Str::limit(Auth::user()->name, 1, '') ) }}
                                        </span>
                                    </div>
                                    @else
                                    <img class="rounded-circle header-profile-user" src="assets/images/users/avatar-4.jpg" alt="Header Avatar">
                                    @endif

                                    <span id="profile-name" class="d-none d-xl-inline-block ml-1 font-weight-medium font-size-15">{{ Auth::user()->name }}</span>
                                    <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <!-- item-->
                                    <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="uil uil-user"></i> <span class="align-middle">View Profile</span></a>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="uil uil-sign-out-alt"></i> <span class="align-middle">{{ __('Logout') }}</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>                 
                                </div>
                            </div>
                    </div>
                </div>
            </header>
            <!-- ========== Left Sidebar Start ========== -->
            <div class="vertical-menu">

                <!-- LOGO -->
                <div class="navbar-brand-box">
                    <a href="{{ url('/') }}" class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="{{ asset('assets/images/logo-sm.png') }}" alt="{{ config('app.name', 'Laravel') }}" height="40">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ asset('assets/images/logo-dark.png') }}" alt="{{ config('app.name', 'Laravel') }}" height="40">
                        </span>
                    </a>

                </div>

                <div data-simplebar class="sidebar-menu-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu list-unstyled" id="side-menu">
                            <li class="menu-title">{{ __('Overview') }}</li>

                            <li>
                                <a href="{{ route('dashboard') }}">
                                    <i class="uil uil-create-dashboard"></i>
                                    <span>{{ __('Dashboard') }}</span>
                                </a>
                            </li>
 
                            <li>
                                <a href="#">
                                    <i class="uil-medical-square"></i>
                                    <span>{{ __('Service Booking') }}</span>
                                </a>
                            </li>

                            <li class="menu-title">{{ __('Phone') }}</li>
 
                            <li>
                                <a href="#">
                                    <i class="uil-phone-alt"></i>
                                    <span>{{ __('Calls') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="uil-forwaded-call"></i><span class="badge badge-pill badge-primary float-right">01</span>
                                    <span>{{ __('Callback') }}</span>
                                </a>
                            </li>


                            <li class="menu-title">{{ __('E-commerce') }}</li>

                            <li>
                                <a href="#">
                                    <i class="uil-shop"></i>
                                    <span>{{ __('Partners') }}</span>
                                </a>
                            </li>


                            <li>
                                <a href="#">
                                    <i class="uil-medkit"></i>
                                    <span>{{ __('Service') }}</span>
                                </a>
                            </li>


                            <li>
                                <a href="#">
                                    <i class="uil-filter"></i>
                                    <span>{{ __('Service Categories') }}</span>
                                </a>
                            </li>

                            <li class="menu-title">{{ __('Promotion') }}</li>

                            <li>
                                <a href="#">
                                    <i class="uil-megaphone"></i>
                                    <span>{{ __('Campaign') }}</span>
                                </a>
                            </li>
 
                            <li class="menu-title">{{ __('Reports') }}</li>

                            <li>
                                <a href="#">
                                    <i class="uil-shopping-cart-alt"></i>
                                    <span>{{ __('Sales') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="uil-balance-scale"></i>
                                    <span>{{ __('Settlement') }}</span>
                                </a>
                            </li>

                            <li class="menu-title">{{ __('Master') }}</li>

                            <li>
                                <a href="#">
                                    <i class="uil-location-point"></i>
                                    <span>{{ __('Locations') }}</span>
                                </a>
                            </li>

                            <li class="menu-title">{{ __('Manage') }}</li>

                            <li>
                                <a href="#">
                                    <i class="uil-users-alt"></i>
                                    <span>{{ __('Users') }}</span>
                                </a>
                            </li>



                            <li>
                                <a href="#">
                                    <i class="uil-shield"></i>
                                    <span>{{ __('Roles') }}</span>
                                </a>
                            </li>


                            <li>
                                <a href="#">
                                    <i class="uil-shield-check"></i>
                                    <span>{{ __('Permissions') }}</span>
                                </a>
                            </li>
 

                        </ul>
                    </div>
                    <!-- Sidebar -->
                </div>
            </div>
            <!-- Left Sidebar End -->

 
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">
                        @yield('content')
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->
 

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <script>document.write(new Date().getFullYear())</script> © How R You.
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-right d-none d-sm-block powered">
                                    Powered By <a href="https://oktopus.tech/" target="_blank" class="text-reset">Oktopus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

 
        <!-- JAVASCRIPT -->
        <script src="{{ mix('assets/js/theme.js') }}"></script>
        <script src="{{ mix('assets/js/app.js') }}"></script>
        
    </body>
</html>
