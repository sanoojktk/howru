<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <!-- App Css-->
        <link href="{{ mix('assets/css/app.css') }}" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body>


    <body class="authentication-bg">

        <div class="home-btn d-none d-sm-block">
            <a href="{{ url('/') }}" class="text-dark"><i class="mdi mdi-home-variant h2"></i></a>
        </div>
        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a href="{{ url('/') }}" class="mb-5 d-block auth-logo">
                                <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="44" class="logo logo-dark">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        @yield('content')
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>

    </body>

</body>
</html>



