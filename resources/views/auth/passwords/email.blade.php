@extends('layouts.auth')

@section('content')
    <div class="card">
        
        <div class="card-body p-4"> 
            <div class="text-center mt-2">
                <h5 class="text-primary">{{ __('Reset Password') }}</h5>
            </div>
            
            <div class="p-2 mt-4">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
 
                    <div class="mt-3 text-right">
                        <button type="submit" class="btn btn-primary w-sm waves-effect waves-light">
                        {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
 

                    <div class="mt-4 text-center">
                        <p class="powered mb-0">Powered By <a href="https://oktopus.tech/" target="_blank" class="text-reset">Oktopus</a></p>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
