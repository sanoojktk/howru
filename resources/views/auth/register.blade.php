@extends('layouts.auth')

@section('content')
    <div class="card">
        
        <div class="card-body p-4"> 
            <div class="text-center mt-2">
                <h5 class="text-primary">{{ __('Register') }}</h5>
            </div>
            <div class="p-2 mt-4">
                <form method="POST" action="{{ route('register') }}">
                   @csrf
                    <div class="form-group">
                        <label for="name">{{ __('Name') }}</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                    


                    <div class="mt-3 text-right">
                        <button type="submit" class="btn btn-primary w-sm waves-effect waves-light">
                            {{ __('Register') }}
                        </button>
                    </div>
 

                    <div class="mt-4 text-center">
                        <p class="mb-0 powered">Powered By <a href="https://oktopus.tech/" target="_blank" class="text-reset">Oktopus</a></p>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
