@extends('layouts.auth')

@section('content')
    <div class="card">
        
        <div class="card-body p-4"> 
            <div class="text-center mt-2">
                <h5 class="text-primary">{{ __('Login') }}</h5>
                <p class="text-muted">{{ __('Login with your \':account\' account', ['account' => config('app.name', 'Laravel')]) }}</p>
            </div>
            <div class="p-2 mt-4">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        @if (Route::has('password.request'))
                        <div class="float-right">
                            <a href="{{ route('password.request') }}" class="text-muted">{{ __('Forgot Your Password?') }}</a>
                        </div>
                        @endif
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="remember">{{ __('Remember Me') }}</label>
                    </div>
                    
                    <div class="mt-3 text-right">
                        <button type="submit" class="btn btn-primary w-sm waves-effect waves-light">
                            {{ __('Login') }}
                        </button>
                    </div>
 

                    <div class="mt-4 text-center">
                        <p class="mb-0 powered">Powered By <a href="https://oktopus.tech/" target="_blank" class="text-reset">Oktopus</a></p>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
