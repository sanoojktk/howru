@extends('layouts.auth')

@section('content')
    <div class="card">
        
        <div class="card-body p-4"> 
            <div class="text-center mt-2">
                <h5 class="text-primary">{{ __('Verify Your Email Address') }}</h5>
            </div>
            <div class="p-2 mt-4">

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif

                {{ __('Before proceeding, please check your email for a verification link.') }}
                {{ __('If you did not receive the email') }},
                <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                   @csrf
                   <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.

                </form>
            </div>

        </div>
    </div>
@endsection
