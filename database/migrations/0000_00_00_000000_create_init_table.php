<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('queue')->index();
            $table->longText('payload');
            $table->unsignedTinyInteger('attempts');
            $table->unsignedInteger('reserved_at')->nullable();
            $table->unsignedInteger('available_at');
            $table->unsignedInteger('created_at');
        });

        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
 
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 100)->index();
            $table->string('type', 100)->default('public');
            $table->timestamps();
        });

        Schema::create('permissions', function (Blueprint $table) {
            $table->bigInteger('role_id')->unsigned();
            $table->string('permission', 100)->index();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('role_id')->unsigned()->nullable(); 
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('mobile', 15)->nullable();
            $table->string('secondary_mobile', 15)->nullable();
            $table->string('address', 15)->nullable();
            $table->string('password', 255);
            $table->text('image')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });

        Schema::create('service_categories', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->string('name', 100);
            $table->text('image')->nullable();
            $table->timestamps();
        });

        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('service_category_id')->unsigned()->nullable(); 
            $table->string('name', 100);
            $table->text('image')->nullable();
            $table->text('details')->nullable();    
            $table->timestamps();
            $table->foreign('service_category_id')->references('id')->on('service_categories')->onDelete('cascade');
        });


        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('partners', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->string('name', 100);
            $table->string('mobile', 15)->nullable();
            $table->text('image')->nullable();
            $table->text('description')->nullable(); 
            $table->timestamps();
        });

        Schema::create('partner_locations', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('partner_id')->unsigned()->nullable(); 
            $table->bigInteger('location_id')->unsigned()->nullable();
            $table->string('address', 255)->nullable();
            $table->integer('settlement_days')->nullable();
            $table->timestamps();

            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
        });

        Schema::create('partner_location_services', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('partner_id')->unsigned()->nullable(); 
            $table->bigInteger('service_id')->unsigned()->nullable();
            $table->float('buying_amount', 8, 2)->default('0');
            $table->float('selling_amount', 8, 2)->default('0');
            $table->float('commission_amount', 8, 2)->default('0');
            $table->timestamps();

            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
        });

        Schema::create('campaigns', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->string('name', 100);
            $table->string('code', 15);
            $table->text('description')->nullable();
            $table->float('discount_percentage', 8, 2)->default('0');
            $table->timestamps();
        });
 
        Schema::create('campaign_services', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('campaign_id')->unsigned()->nullable(); 
            $table->bigInteger('partner_location_service_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->foreign('partner_location_service_id')->references('id')->on('partner_location_services')->onDelete('cascade');
        });
 
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->string('name', 100);
            $table->string('mobile', 15);
            $table->string('secondary_mobile', 15)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('address', 255)->nullable();
            $table->text('remark')->nullable();
            $table->bigInteger('campaign_id')->unsigned()->nullable();
            $table->float('total_amount', 8, 2)->default('0');
            $table->float('received_amount', 8, 2)->default('0');
            $table->string('payment_method', 100)->nullable()->default('0');
            $table->string('created_method', 10);
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->timestamps();


            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->bigInteger('partner_id')->unsigned()->nullable();
            $table->bigInteger('partner_location_id')->unsigned()->nullable();
            $table->bigInteger('partner_location_service_id')->unsigned()->nullable();
            $table->string('partner_name', 100);
            $table->text('partner_image')->nullable();
            $table->text('partner_details')->nullable();
            $table->string('customer_name', 100);
            $table->integer('customer_age')->nullable();
            $table->string('name', 100);
            $table->integer('qty')->default('0');
            $table->text('image')->nullable();
            $table->text('remark')->nullable();
            $table->date('booking_date');
            $table->time('booking_time', 0);
            $table->bigInteger('campaign_id')->unsigned()->nullable();    
            $table->float('buying_amount', 8, 2);
            $table->float('selling_amount', 8, 2);
            $table->float('discount_amount', 8, 2);
            $table->float('commission_amount', 8, 2);
            $table->date('settlement_date');
            $table->string('status', 25);
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
            $table->foreign('partner_location_id')->references('id')->on('partner_locations')->onDelete('cascade');
            $table->foreign('partner_location_service_id')->references('id')->on('partner_location_services')->onDelete('cascade');
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
        });

        Schema::create('order_status', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->text('details');
            $table->date('date');
            $table->time('time', 0);
            $table->string('status', 25);
            $table->timestamps();
        });


        Schema::create('calls', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->string('type', 10);
            $table->string('name', 100);
            $table->string('mobile', 15);
            $table->string('secondary_mobile', 15)->nullable();
            $table->string('email', 100)->nullable();
            $table->bigInteger('campaign_id')->unsigned()->nullable();
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->text('remark')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('callbacks', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->bigInteger('call_id')->unsigned()->nullable();
            $table->date('date');
            $table->time('time', 0);
            $table->text('remark')->nullable();
            $table->string('created_method', 10);
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('call_id')->references('id')->on('calls')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });

 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('failed_jobs');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('users');
        Schema::dropIfExists('service_categories');
        Schema::dropIfExists('services');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('partners');
        Schema::dropIfExists('partner_locations');
        Schema::dropIfExists('partner_location_services');
        Schema::dropIfExists('campaigns');
        Schema::dropIfExists('campaign_services');
        Schema::dropIfExists('calls');
        Schema::dropIfExists('callbacks');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('order_status');

    }
}
